#!/bin/bash

set -ue

function help()
{
  echo "Usage: create-debian machine-name install-image [-r disk-device] [-s disk-size (in GB)]"
  exit 1
}

if [ "$#" -lt 3 ]; then
  help
fi

source ./vars.sh

machine="$1"
shift

install_image="$1"
shift

overwrite=false
disk=""

while getopts "?os:r:" opt; do
    case "$opt" in
    h|\?)
        help
        exit 1
        ;;
    r)  disk="$OPTARG"
        ;;
    o) overwrite=true
        ;;
    s) size="$OPTARG"
    esac
done


function create_vdi_disk() # path
{
  VBoxManage createhd --filename "$1.vdi" --size $(($size * 1024))
  VBoxManage storageattach "$machine" --storagectl "SATA Controller" --port 1 --device 0 --type hdd --medium "$1.vdi"
}

function create_mapped_disk() # path
{
  VBoxManage internalcommands createrawvmdk -filename "$1.vmdk" -rawdisk "$disk"

  VBoxManage storageattach "$machine" --storagectl "SATA Controller" --port 1 --device 0 --type hdd --medium "$1.vmdk"
}

function create_serial_port() # path
{
   VBoxManage modifyvm "$machine" --uart1 0x3F8 4
   VBoxManage modifyvm "$machine" --uartmode1 server "$1/serial"
}

function create_disk() # vm_path
{
  path="$1/disk"

  if [ "$disk" != "" ]; then
    create_mapped_disk "$path"
  else
    create_vdi_disk "$path"
  fi
}

function create_vm()
{
  VBoxManage createvm --name "$machine" --ostype "Debian_64" --register
  VBoxManage storagectl "$machine" --name "SATA Controller" --add sata  --add sata --controller IntelAHCI --portcount 2
  VBoxManage storageattach "$machine" --storagectl  "SATA Controller" --port 0 --device 0 --type dvddrive --medium "$install_image"
  VBoxManage modifyvm "$machine" --ioapic on
  VBoxManage modifyvm "$machine" --boot1 dvd --boot2 disk --boot3 none --boot4 none
  VBoxManage modifyvm "$machine" --memory $memory --vram $vram --cpus $cpus
  VBoxManage modifyvm "$machine" --natpf1 "guestssh,tcp,,2200,,22"
}

function start_vm()
{
  VBoxManage startvm "$machine"
}

# Generate the keys using sudo showkeys -s
function play_keys()
{
  echo "Playing keys: $1"
  for word in $(<keys/$1.key); do
    VBoxManage controlvm "$machine" keyboardputscancode  "$word"
    sleep 0.05
  done
}

function tail_logs() #vm_path
{
  socat - UNIX-CONNECT:"$1/serial"
}

create_vm
vm_file=$(VBoxManage showvminfo "$machine" --machinereadable | grep CfgFile | sed s/CfgFile=//g | sed s/\"//g)
vm_dir=$(dirname "$vm_file")

create_disk "$vm_dir"
create_serial_port "$vm_dir"
start_vm
sleep 10

play_keys esc
play_keys install-params
play_keys space
play_keys text
play_keys space
play_keys hostname-params
play_keys space
play_keys preseed-params
play_keys space
play_keys console-params
play_keys 'return'


tail_logs "$vm_dir"

#play_keys tail-logs


