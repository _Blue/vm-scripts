#! /bin/bash

set -ue
TMP_FILE=tmp

function clean_file()
{
  lines=$(wc -l < "$1")


  if [ "$lines" -lt 2 ]; then
    echo "$1: PASS"
    return
  fi
  cat "$1" | grep '.*0x.*' | sed s/0x//g | sed s/1d//g | tr '\n' ' ' > "$TMP_FILE"

  mv "$TMP_FILE" "$1"
}

for file in *.key; do
  clean_file "$file"
done
